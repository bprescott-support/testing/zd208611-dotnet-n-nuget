
## purpose

project creates a nuget package.

it'll be called `NetCore.Docker`, and will be version `1.0.0`:  `netcore.docker.1.0.0.nupkg`

this comes from `testapp/NetCore.Docker.csproj` which in turn came from running:

```
dotnet new console -o App -n NetCore.Docker
```

## stash

- app came from https://docs.microsoft.com/en-us/dotnet/core/docker/build-container?tabs=linux

```
dotnet new console -o App -n NetCore.Docker
```

- `dotnet nuget` came from https://bitbucket.org/certusportautomation/bitbucket-pipe-dotnet-publish/src/master/pipe.sh

- there's an example of using `dotnet nuget` [in our docs](https://docs.gitlab.com/ee/user/packages/nuget_repository/#publish-a-nuget-package-by-using-cicd)


```
#  add      Add a NuGet source.
  # --configfile     The NuGet configuration file. If specified, only the settings from this
  #                  file will be used. If not specified, the hierarchy of configuration files
  #                  from the current directory will be used. For more information, see
  #                  https://docs.microsoft.com/nuget/consume-packages/configuring-nuget-behavior.
#  delete   Deletes a package from the server.
#  disable  Disable a NuGet source.
#  enable   Enable a NuGet source.
#  list     List configured NuGet sources.
#  locals   Clears or lists local NuGet resources such as http requests cache, packages folder, plugin operations cache  or machine-wide global packages folder.
#  push     Pushes a package to the server and publishes it.
#  remove   Remove a NuGet source.
#  update   Update a NuGet source.
#  verify   Verifies a signed NuGet package.

# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-pack
```
